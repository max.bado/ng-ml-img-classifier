import { TestBed } from '@angular/core/testing';

import { ClassifierModelService } from './classifier-model.service';

describe('ClassifierModelService', () => {
  let service: ClassifierModelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClassifierModelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
