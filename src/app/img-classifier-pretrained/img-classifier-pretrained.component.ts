import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as mobilenet from '@tensorflow-models/mobilenet';
import * as tf from '@tensorflow/tfjs';
import {MobileNet} from "@tensorflow-models/mobilenet";
import * as knn from '@tensorflow-models/knn-classifier';
import {interval, Subscription} from "rxjs";
import {FormBuilder, FormGroup} from "@angular/forms";
import {FileSystemDirectoryEntry, FileSystemFileEntry, NgxFileDropEntry} from "ngx-file-drop";
import {CompressorService} from "../compressor.service";
import {map} from "rxjs/operators";
import {ImageObject} from "../img-classifier/img-classifier.component";
import {Prediction} from "../prediction";

@Component({
  selector: 'app-img-classifier-pretrained',
  templateUrl: './img-classifier-pretrained.component.html',
  styleUrls: ['./img-classifier-pretrained.component.scss']
})
export class ImgClassifierPretrainedComponent implements OnInit {

  @ViewChild("chosenImage") imageEl: ElementRef;
  // @ViewChild("fileUpload") fileUpload: ElementRef;

  mobilenet: MobileNet;
  predictions: Prediction[];
  classPred: Prediction;
  imageSrc: string;
  classifier = knn.create();

  public validationFiles: NgxFileDropEntry[] = [];

  constructor() { }

  async ngOnInit(): Promise<void> {

    this.mobilenet = await mobilenet.load();

  }

  public predictDropped(validationFiles: NgxFileDropEntry[]) {
    this.validationFiles = validationFiles;

    for (const droppedFile of validationFiles) {
      if (droppedFile.fileEntry.isFile) {
        // console.log('isFile');
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          const reader = new FileReader();

          reader.onload = (res: any) => {
            console.log('reader res: ', res);
            this.imageSrc = res.target.result;
            this.imageEl.nativeElement.src = this.imageSrc;
            setTimeout(async () => {
              const imgEl = this.imageEl.nativeElement;
              this.predictions = await this.mobilenet.classify(imgEl);

              if (this.classifier.getNumClasses() > 0) {
                const img = this.imageEl.nativeElement;

                // Get the activation from mobilenet from the webcam.
                const activation = this.mobilenet.infer(img);
                // Get the most likely class and confidence from the classifier module.
                const result = await this.classifier.predictClass(activation);
                console.log(result);

                const classes = ['A', 'B', 'C'];
                const classPred: Prediction = {
                  probability: 0,
                  className: null
                };
                classPred.className = classes[result.label];
                classPred.probability = result.confidences[result.label];
                this.classPred = classPred;

              }
            }, 0)
          };

          reader.readAsDataURL(file);
        });
      } else {
        // console.log('isDirectory');
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        // console.log(droppedFile.relativePath, fileEntry);
      }

    }

    console.log('done parsing dropped prediction files');
    return true;
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }

}
