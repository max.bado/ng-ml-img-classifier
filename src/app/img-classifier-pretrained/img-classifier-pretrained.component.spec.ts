import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgClassifierPretrainedComponent } from './img-classifier-pretrained.component';

describe('ImgClassifierPretrainedComponent', () => {
  let component: ImgClassifierPretrainedComponent;
  let fixture: ComponentFixture<ImgClassifierPretrainedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgClassifierPretrainedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgClassifierPretrainedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
