import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  url$ = this.router.events.pipe(
    map((url) => {
      // console.log(url);
      return this.router.url;
    })
  )

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.router.navigate(['/pre-trained']);
  }

}
