import {Injectable, OnInit} from '@angular/core';
import * as tf from '@tensorflow/tfjs';

@Injectable({
  providedIn: 'root'
})
export class ClassifierModelService {

  public pretrainedModel;

  constructor() { }

  loadMobilenet() {
    console.log('load mobilenet');
    return tf.loadLayersModel('https://storage.googleapis.com/tfjs-models/tfjs/mobilenet_v1_0.25_224/model.json');
  }

  buildPretrainedModel() {
    console.log('build pretrained model');

    return this.loadMobilenet().then(mobilenet => {
      const layer = mobilenet.getLayer('conv_pw_13_relu');
      return tf.model({
        inputs: mobilenet.inputs,
        outputs: layer.output,
      });
    });
  }
}
