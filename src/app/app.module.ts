import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatCardModule} from "@angular/material/card";



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ImgClassifierComponent } from './img-classifier/img-classifier.component';
import { HeaderComponent } from './header/header.component';
import {NgxFileDropModule} from "ngx-file-drop";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule} from "@angular/material/dialog";
import {MatListModule} from "@angular/material/list";
import {MatInputModule} from "@angular/material/input";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatRadioModule} from "@angular/material/radio";
import { ImgClassifierPretrainedComponent } from './img-classifier-pretrained/img-classifier-pretrained.component';

@NgModule({
  declarations: [
    AppComponent,
    ImgClassifierComponent,
    HeaderComponent,
    ImgClassifierPretrainedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxFileDropModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatRadioModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
