import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ImgClassifierComponent} from "./img-classifier/img-classifier.component";
import {ImgClassifierPretrainedComponent} from "./img-classifier-pretrained/img-classifier-pretrained.component";


const routes: Routes = [
  { path: 'self-trained', component: ImgClassifierComponent },
  { path: 'pre-trained', component: ImgClassifierPretrainedComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
