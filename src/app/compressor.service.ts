import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CompressorService {

  constructor() { }
  compress(file: File, inputSize: number): Observable<any> {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    return Observable.create(observer => {
      reader.onload = ev => {
        const img = new Image();
        img.src = (ev.target as any).result;

        (img.onload = () => {
          const elem = document.createElement('canvas'); // Use Angular's Renderer2 method
          // console.log('image width: ', img.width);
          // console.log('image height: ', img.height);

          const scale = inputSize / Math.max(img.height, img.width)
          const width = scale * img.width
          const height = scale * img.height
          // console.log('new image width: ', width);
          // console.log('new image height: ', height);
          // const scaleFactor = width / img.width;
          // elem.width = width;
          // elem.height = img.height * scaleFactor;
          elem.width = inputSize;
          elem.height = inputSize;
          const ctx = <CanvasRenderingContext2D>elem.getContext('2d');
          // ctx.drawImage(img, 0, 0, width, img.height * scaleFactor);
          ctx.drawImage(img, 0, 0, width, height);
          observer.next(ctx.canvas.toDataURL());

        }),
          (reader.onerror = error => observer.error(error));
      };
    });
  }

}
