import { Component, OnInit } from '@angular/core';
import {EMPTY, interval, Subject, Subscription, timer} from 'rxjs';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import * as tf from '@tensorflow/tfjs';
import {FormBuilder, FormGroup} from "@angular/forms";
import {CompressorService} from "../compressor.service";
import {expand, map} from "rxjs/operators";
import {ClassifierModelService} from "../classifier-model.service";

export interface ImageObject {
  name: string,
  path: string,
  image: any,
  class: string,
}

@Component({
  selector: 'app-img-classifier',
  templateUrl: './img-classifier.component.html',
  styleUrls: ['./img-classifier.component.scss']
})
export class ImgClassifierComponent implements OnInit {

  // tf;
  errorMessage = null;
  batchesDropped = 0;
  training = [];
  trainingImages: ImageObject[] = [];
  validation = [];
  validationImg;

  trainingLabels = [];
  validationLabels = [];

  model;
  modelTrained = false;
  dataLoaded = false;
  pretrainedModel;
  trainingImgUrls = [];
  validationImgUrls = [];
  predictionLabels = [];
  processingImage;
  labelObject = {};
  uniqueLabels = [];
  progressValue = 0;
  progressUpdateInterval = 5000;
  handleProgress$ = new Subscription();
  currentIndex = 1;
  uploadProgressValue = 0;
  uploadComplete = false;
  handleUpload$ = new Subscription();

  phase = 'Not Started';

  currentBackend;
  optionsForm: FormGroup;
  defaultEpochs = 5;
  defaultBatchSize = 32;
  defaultBackendType = 'webgl';

  public trainingFiles: NgxFileDropEntry[] = [];
  public validationFiles: NgxFileDropEntry[] = [];

  compressedImages = [];
  inputSize = 224;

  constructor(
    private formBuilder: FormBuilder,
    private compressor: CompressorService,
    private classifierModelService: ClassifierModelService
  ) { }

  async ngOnInit(): Promise<void> {

    this.initOptionsForm();

    if (!this.classifierModelService.pretrainedModel) {
      console.log('Building pre-trained model');
      this.classifierModelService.pretrainedModel = await this.classifierModelService.buildPretrainedModel();
      this.pretrainedModel = this.classifierModelService.pretrainedModel
      console.log('Finished building pre-trained model');
    }

    // Helps avoid memory leak?
    // tf.ENV.set('WEBGL_CONV_IM2COL', false);

    this.currentBackend = tf.getBackend();
  }

  initOptionsForm() {
    this.optionsForm = this.formBuilder.group({
      epochs: [this.defaultEpochs],
      batchSize: [this.defaultBatchSize]
    })
  }

  async changeBackend(type: string) {
    console.log('old backend type: ', this.currentBackend);
    if (this.currentBackend !== type) {
      this.currentBackend = type;
      await tf.setBackend(this.currentBackend);
      console.log('new backend type: ', this.currentBackend);
    } else {
      console.log('backend type already: ', type);
    }
  }

  async reset() {
    this.handleUpload$.unsubscribe();
    this.handleProgress$.unsubscribe();
    this.uploadComplete = false;
    this.uploadProgressValue = 0;
    this.errorMessage = null;
    this.batchesDropped = 0;
    this.validationImgUrls = [];
    this.predictionLabels = [];
    this.processingImage = null;
    this.labelObject = {};
    this.uniqueLabels = [];
    this.progressValue = 0;
    this.currentIndex = 1;
    this.phase = 'Not Started';
    this.compressedImages = [];

    this.training = [];
    this.trainingImages = [];
    this.validation = [];
    this.validationFiles = [];
    this.validationImg = null;

    this.trainingLabels = [];
    this.validationLabels = [];

    this.model = null;
    this.modelTrained = false;
    this.dataLoaded = false;
    this.initOptionsForm();
    tf.disposeVariables();
    this.pretrainedModel = await this.buildPretrainedModel();
    await tf.setBackend(this.defaultBackendType);
  }

  async trainModel(trainingData: ImageObject[]) {
    console.log('train model');
    if (this.errorMessage) {
      console.log('re-initializing the pre-trained model');
      tf.disposeVariables();
      this.pretrainedModel = await this.buildPretrainedModel();
      await tf.setBackend(this.currentBackend);
      this.errorMessage = null;
    }
    // console.log('training: ', training);
    const training = trainingData.map(x => x.image);
    // console.log('training: ', training);
    const trainingLabels = trainingData.map(x => x.class);
    console.log('labels: ', trainingLabels);

    this.phase = 'Processing Images';
    try {
      const xs = await this.loadImages(training, this.pretrainedModel);
      console.log('xs: ', xs);
      this.handleProgress$.unsubscribe();
      this.progressValue = 50;

      const ys = this.addLabels(trainingLabels);
      this.progressValue = 60;

      const classNum = Object.keys(this.labelObject).length;
      // console.log('label object: ', classNum);
      const model = this.getModel(classNum);
      this.model = model;
      this.progressValue = 75;

      const formBatchSize = this.optionsForm.controls.batchSize.value;
      const epochs = this.optionsForm.controls.epochs.value;
      const batchSize = this.getBatchSize(formBatchSize, xs);

      this.phase = 'Training Model';
      console.log('epochs: ', epochs);
      console.log('batchSize: ', batchSize);
      const history = await this.model.fit(xs, ys, {
        epochs: epochs,
        batchSize: batchSize,
        shuffle: true,
      });

      this.progressValue = 100;
      this.phase = 'Training Finished';
      this.modelTrained = true;

      console.log('history: ', history);

      return {
        model,
        history
      };
    } catch (e) {
      this.handleProgress$.unsubscribe();
      this.errorMessage = 'Oh no! We crashed! If you ran out of memory, try rerunning the training with a different backend!';
      console.error(e);
    }
  }

  getTfMemoryStats() {
    const memory = tf.memory() as any;
    return {
      numBytes: memory.numBytes,
      numBytesInGPU: memory.numBytesInGPU,
      numTensors: memory.numTensors
    }
  }

  getBatchSize(batchSize?: number, xs?: tf.Tensor) {
    if (batchSize) {
      return batchSize;
    }

    if (xs !== undefined) {
      return Math.floor(xs.shape[0] * 0.4) || 1;
    }

    return undefined;
  }

  public trainDropped(files: NgxFileDropEntry[]) {
    // this.trainingImages = [];
    // this.training = [];
    // this.trainingLabels = [];
    this.trainingFiles = files;
    // this.dataLoaded = false;
    this.modelTrained = false;
    this.batchesDropped++;
    this.uploadComplete = false;
    this.handleUpload(files, this.trainingImages);
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        // console.log('isFile');
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          // console.log(droppedFile.relativePath, file);
          // console.log('file size: ', file.size);

          const reader = new FileReader();

          reader.readAsDataURL(file);
          reader.onload = (e) => {
            const imgUrl = reader.result;
            this.addTrainingItem(droppedFile, imgUrl, file);
            // this.trainingImgUrls.push(imgUrl)
          };
          // this.readURL(file);

        });
      } else {
        // console.log('isDirectory');
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        // console.log(droppedFile.relativePath, fileEntry);
      }
    }

    this.dataLoaded = true;
    // console.log('done parsing dropped training files');
    return true;
  }

  public predictDropped(validationFiles: NgxFileDropEntry[]) {
    this.validationFiles = validationFiles;

    for (const droppedFile of validationFiles) {
      if (droppedFile.fileEntry.isFile) {
        // console.log('isFile');
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          // console.log(droppedFile.relativePath, file);

          const reader = new FileReader();

          reader.readAsDataURL(file);
          reader.onload = (e) => {
            const imgUrl = reader.result;
            let className = '';

            this.validationImgUrls = [];
            this.validationImgUrls.push(imgUrl)
            this.makePrediction(this.pretrainedModel, imgUrl, className);
          };

        });
      } else {
        // console.log('isDirectory');
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        // console.log(droppedFile.relativePath, fileEntry);
      }

    }

    console.log('done parsing dropped prediction files');
    return true;
  }

  getClassItems(dataset: ImageObject[], className: string): ImageObject[] {
    const result = dataset.filter(x => x.class === className);

    return result;
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }

  addTrainingItem(dropFile: NgxFileDropEntry, image, file: File) {
    const name = dropFile.fileEntry.name;
    const path = dropFile.relativePath;
    const nameSplit = dropFile.relativePath.split('/');
    let className;
    if (nameSplit.length > 1) {
      className = nameSplit[(nameSplit.length - 2)]
    } else {
      const batchNum = this.batchesDropped;
      className = `unknown batch ${batchNum}`;
      console.log('Unable to determine class name for image: ', name);
    }

    const compress = this.compressor.compress(file, this.inputSize).pipe (
      map(response => {

        //Code block after completing each compression
        // console.log('compressed ' + file.name);
        this.compressedImages.push(response);
        return {
          data: response,
          // index: index + 1,
          // array: array,
        };
      }),
    );

    compress.subscribe(res => {
      // console.log('compressed: ', res);

      const resizedImage = res.data;
      // console.log('resized image: ', resizedImage);

      const imageObject: ImageObject = {
        name: name,
        path: path,
        image: resizedImage,
        class: className,
      }

      this.trainingImages.push(imageObject);
      // this.training.push(resizedImage);
      // this.trainingLabels.push(className);

      const label = className;
      if (this.labelObject[label] === undefined) {
        // only assign it if we haven't seen it before
        this.labelObject[label] = Object.keys(this.labelObject).length;
        this.uniqueLabels.push(label);
      }

      // console.log('added training item: ', className, name);
    })
  }

  getUnknownClasses(labelObject) {
    const labels = Object.keys(labelObject);
    return labels.filter(x => x.match('unknown batch'))
  }

  setClass(oldClass: string, newClass: string) {
    // console.log(`changing class '${oldClass}' to class '${newClass}'`)
    // console.log('old label object: ', this.labelObject);
    this.labelObject[newClass] = this.labelObject[oldClass];
    // console.log('new label object: ', this.labelObject);
    delete this.labelObject[oldClass];
    // console.log('new label object after delete: ', this.labelObject);
    // console.log('old training images: ', this.trainingImages);

    for (let i = 0; i < this.trainingImages.length; i++) {
      if (this.trainingImages[i].class === oldClass) {
        this.trainingImages[i].class = newClass;
      }
    }

    // console.log('new training images: ', this.trainingImages);

    for (let i = 0; i < this.uniqueLabels.length; i++) {
      if (this.uniqueLabels[i] === oldClass) {
        this.uniqueLabels[i] = newClass;
      }
    }

  }

  addValidationItem(file: NgxFileDropEntry, image) {
    const className = file.relativePath.split('/')[0];
    const name = file.fileEntry.name;

    this.validation = [];
    this.validation.push(image);

    this.validationLabels = [];
    this.validationLabels.push(className);

    // console.log('added validation item: ', className, name);
    // return { className: className };
  }

  loadMobilenet() {
    console.log('load mobilenet');

    return tf.loadLayersModel('https://storage.googleapis.com/tfjs-models/tfjs/mobilenet_v1_0.25_224/model.json');
  }


  loadImage(src) {
    // console.log('load image');
    this.processingImage = src;
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.src = src;
      img.onload = () => resolve(tf.browser.fromPixels(img));
      img.onerror = (err) => reject(err);
    });
  }

  cropImage(img) {
    // console.log('crop image');
    const width = img.shape[0];
    const height = img.shape[1];

    // use the shorter side as the size to which we will crop
    const shorterSide = Math.min(img.shape[0], img.shape[1]);

    // calculate beginning and ending crop points
    const startingHeight = (height - shorterSide) / 2;
    // console.log('starting height: ', startingHeight);
    const startingWidth = (width - shorterSide) / 2;
    // console.log('starting width: ', startingWidth);
    const endingHeight = Math.floor(startingHeight + shorterSide);
    // console.log('ending height: ', endingHeight);
    const endingWidth = Math.floor(startingWidth + shorterSide);
    // console.log('ending width: ', endingWidth);

    // return image data cropped to those points
    return img.slice([startingWidth, startingHeight, 0], [endingWidth, endingHeight, 3]);
  }

  resizeImage(image) {
    // console.log('resize image');
    return tf.image.resizeBilinear(image, [224, 224]);
  }

  batchImage(image) {
    // console.log('batch image: ', image);
    // Expand our tensor to have an additional dimension, whose size is 1
    const batchedImage = image.expandDims(0);

    // Turn pixel data into a float between -1 and 1.
    return batchedImage.toFloat().div(tf.scalar(127)).sub(tf.scalar(1));
  }

  loadAndProcessImage(image, phase) {
    // console.log('load and process image: ', image);

    let batchedImage;
    if (phase === 'training') {
      batchedImage = this.batchImage(image);
    } else {
      const croppedImage = this.cropImage(image);
      const resizedImage = this.resizeImage(croppedImage);
      batchedImage = this.batchImage(resizedImage);
    }

    return batchedImage;
  }


  buildPretrainedModel() {
    console.log('build pretrained model');

    return this.loadMobilenet().then(mobilenet => {
      const layer = mobilenet.getLayer('conv_pw_13_relu');
      return tf.model({
        inputs: mobilenet.inputs,
        outputs: layer.output,
      });
    });
  }

  handleProgress(dataset: any[]) {
    this.handleProgress$ = interval(this.progressUpdateInterval)
      .subscribe(val => {
        // console.log('interval: ', val);
        this.progressValue = this.updateImageProcessProgress(dataset, this.currentIndex);
        // console.log('tf memory usage: ', tf.memory());
      });
  }

  updateImageProcessProgress(dataset: any[], index: number) {
    return (index / dataset.length) * 50;
  }

  handleUpload(sourceDataset: any[], targetDataset: any[]) {
    this.handleUpload$ = interval(this.progressUpdateInterval)
      .subscribe(val => {
        // console.log('interval: ', val);
        this.uploadProgressValue = this.updateUploadProgress(sourceDataset, targetDataset);
        // console.log('tf memory usage: ', tf.memory());
        if (sourceDataset.length === targetDataset.length) {
          this.uploadComplete = true;
          this.handleUpload$.unsubscribe();
        }
      });
  }
  updateUploadProgress(sourceDataset: any[], targetDataset: any[]) {
    return (targetDataset.length / sourceDataset.length) * 100;
  }

  loadImages(images, pretrainedModel): Promise<any> {
    // console.log('load images: ', images);
    // console.log('labels: ', this.trainingLabels);
    let promise = Promise.resolve();
    this.handleProgress(images);
    for (let i = 0; i < images.length; i++) {
      const image = images[i];

      promise = promise.then((data: any) => {
        // console.log('data: ', data);
        // this.progressValue = this.updateProgress(images, i);
        // console.log('image index: ', i);
        // console.log('progress value: ', this.progressValue);
        this.currentIndex = i+1;
        return this.loadImage(image).then(loadedImage => {
          // console.log('loaded image: ', loadedImage);
          // Note the use of `tf.tidy` and `.dispose()`. These are two memory management
          // functions that Tensorflow.js exposes.
          // https://js.tensorflow.org/tutorials/core-concepts.html
          //
          // Handling memory management is crucial for building a performant machine learning
          // model in a browser.
          return tf.tidy(() => {
            const processedImage = this.loadAndProcessImage(loadedImage, 'training');

            // console.log('processed image: ', processedImage);
            const prediction = pretrainedModel.predict(processedImage);
            // console.log('prediction: ', prediction);

            if (data) {
              const newData = data.concat(prediction);
              // console.log('new data: ', newData);
              data.dispose();
              // tf.dispose();
              // console.log('returning new data: ', newData);
              return newData;
            }

            return tf.keep(prediction);
          });
        });
      });
    }

    return promise;
  }

  // I was trying to work around the memory leak issues... nothing helped!
  disposeTensors() {
    let backendType = tf.getBackend();
    console.log('current backend type: ', backendType);
    let backend = new tf.webgl.MathBackendWebGL();
    console.log('backend: ', backend);
    tf.dispose();
    tf.disposeVariables();
    backendType = tf.getBackend();
    console.log('current backend type after dispose: ', backendType);

    let newBackend = new tf.webgl.MathBackendWebGL();
    console.log('backend after dispose: ', newBackend);
    // backend.dispose();
    // tf.disposeVariables();
    // backend = new tf.webgl.MathBackendWebGL();


    // tf.backend().dispose();
  }

  oneHot(labelIndex, classLength) {
    // console.log('one hot');
    return tf.tidy(() => tf.oneHot(tf.tensor1d([labelIndex]).toInt(), classLength));
  }

  getLabelsAsObject(labels) {
    console.log('get labels as object: ', labels);
    let labelObject = {};
    for (let i = 0; i < labels.length; i++) {
      const label = labels[i];
      if (labelObject[label] === undefined) {
        // only assign it if we haven't seen it before
        labelObject[label] = Object.keys(labelObject).length;
        this.uniqueLabels.push(label);
      }
    }
    console.log('label object: ', labelObject);
    this.labelObject = labelObject;
    return labelObject;
  }

  addLabels(labels) {
    console.log('add labels: ', labels);
    this.phase = 'Adding Labels';
    return tf.tidy(() => {
      // const classes = this.getLabelsAsObject(labels);
      const classes = this.labelObject;
      const classLength = Object.keys(classes).length;

      let ys;
      for (let i = 0; i < labels.length; i++) {
        const label = labels[i];
        const labelIndex = classes[label];
        const y = this.oneHot(labelIndex, classLength);
        if (i === 0) {
          ys = y;
        } else {
          ys = ys.concat(y, 0);
        }
      }
      return ys;
    });
  }

  getModel(numberOfClasses) {
    console.log('get model: ', numberOfClasses);
    this.phase = 'Creating Model';
    const model = tf.sequential({
      layers: [
        tf.layers.flatten({ inputShape: [7, 7, 256] }),
        tf.layers.dense({
          units: 100,
          activation: 'relu',
          kernelInitializer: 'varianceScaling',
          useBias: true
        }),
        tf.layers.dense({
          units: numberOfClasses,
          kernelInitializer: 'varianceScaling',
          useBias: false,
          activation: 'softmax'
        })
      ],
    });

    this.phase = 'Compiling Model';
    model.compile({
      optimizer: tf.train.adam(0.0001),
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy'],
    });

    return model;
  }

  makePrediction(pretrainedModel, image, expectedLabel) {
    console.log('make prediction');
    this.loadImage(image).then(loadedImage => {
      return this.loadAndProcessImage(loadedImage, 'prediction');
    }).then(loadedImage => {
      // console.log('loaded image: ', loadedImage);
      const activatedImage = pretrainedModel.predict(loadedImage);
      loadedImage.dispose();
      return activatedImage;
    }).then(activatedImage => {
      // console.log('activated image: ', activatedImage);
      const prediction = this.model.predict(activatedImage);
      const predictionLabel = prediction.as1D().argMax().dataSync()[0];

      // expectedLabel = 0;

      // const keys = Object.keys(this.labelObject);
      const predictionClass = this.getKeyByValue(this.labelObject, predictionLabel);
      this.predictionLabels = [];
      this.predictionLabels.push(predictionClass);
      console.log('Expected Label: ', expectedLabel);
      console.log('Predicted Label: ', predictionClass);
      console.log('Prediction: ', prediction);

      prediction.dispose();
      activatedImage.dispose();
    });
  }

  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

}
