# In-Browser Image Classifier Angular App

This project is a simple in-browser image classifier using TensorFlow.js and Angular. The application takes an input of labeled images, trains an image classification model, and then predicts the label of uploaded validation images.

## Live App

The app is live at the following location: [https://d4d9oq9jm20ve.cloudfront.net/](https://d4d9oq9jm20ve.cloudfront.net/)

## Environment Setup

In order to run this project on a local machine, you need the following:
*  Node.js - Download and install the Node.js runtime from [here](https://nodejs.org/en/download/)
*  Angular CLI - Once you've installed Node.js, install the Angular CLI using the following command: `npm install -g @angular/cli`

## Running the App

Once the environment is configured...
*  Clone this repository...
   *  via HTTPS: `git clone https://gitlab.com/max.bado/ng-ml-img-classifier.git`
   *  via SSH: `git clone git@gitlab.com:max.bado/ng-ml-img-classifier.git`
   *  download and extract: [zip](https://gitlab.com/max.bado/ng-ml-img-classifier/-/archive/master/ng-ml-img-classifier-master.zip)
*  Navigate to the root of the repository and...
   *  Run `npm install`
   *  Run `ng serve`
*  Open your web browser to [http://localhost:4200](http://localhost:4200)

## Using the App

#### Upload Training Images
To start using the app, you need to upload a set of images by dragging the folder or folders containing the images into the app's `training image dropzone`. You can use the datasets provided within this repository. You can find sample datasets within the [data folder]().
![image1](docs/2020-05-05 15_32_37-Greenshot.png)

*  The app will classify the uploaded images by the name of their containing folder. 
   *  For example, if you upload 2 folders, `dogs` and `cats`, and each folder contains 100 images, the app will classify the 100 images contained within the `dogs` folder as `dogs` and the 100 images contained within the `cats` folder as `cats`.
   *  You can upload a folder containing subfolders. For example, you can upload a folder, `training` containing subfolders, `chairs`, `umbrellas`, and `cars`, each containing images.  
   *  If you upload individual images instead of subfolders, the app classify these images as `unknown batch n`. The app will prompt you to attach a classification to each uploaded batch of images.
   
#### Inspect Training Dataset
Once images have been uploaded, you can view the uploaded images by expanding the corresponding class dropdown within the Training Dataset card. 
![image2](docs/2020-05-05 16_00_22-NgMlImgClassifier.png)

#### Train the Model
You can now begin training the model. Before you start, you can change the amount of Epochs, Batch Size, and whether to use the CPU or GPU to do the processing. Click the `Train Model` button to begin.
NOTE: The app uses the `mobilenet` image classification model. Unfortunately, this model has performance and memory leak issues working with large datasets when using a GPU backend. The app gives you the option to switch to using a CPU backend, which, while slower, is more stable and is better at avoiding crashes.
![image3](docs/2020-05-05 16_01_21-NgMlImgClassifier.png)

#### Make Prediction
Once the model is trained, you can drop an individual image into the app's `validation image dropzone`.
![image4](docs/2020-05-05 16_02_09-Greenshot.png)

The app will display the uploaded image with a class prediction.
![image5](docs/2020-05-05 16_05_01-NgMlImgClassifier.png)

To start over with a new dataset, click the `Start Over` button.
